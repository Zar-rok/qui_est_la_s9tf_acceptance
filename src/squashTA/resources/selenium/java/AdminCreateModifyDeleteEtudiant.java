import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AdminCreateModifyDeleteEtudiant {

    private final Logger logger = Logger.getLogger(AdminCreateModifyDeleteEtudiant.class.getName());
    private WebDriver driver;
    private String urlRoot;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        urlRoot = "http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test4/preprod/Source/Vendor/";
    }

    @Test
    public void testAdminCreateModifyDeleteEtudiant() {
        final String initialNomPrenom = "initial";
        final String initialEmail = "ini@test.ts";
        final String modifiedNomPrenom = "modified";
        final String modifiedEmail = "mod@test.ts";

        driver.get(urlRoot + "controllers/connexion.php");
        driver.findElement(By.id("idEmail")).sendKeys("testCO@co.ts");
        driver.findElement(By.id("idMotDePasse")).sendKeys("pouet");
        driver.findElement(By.name("submit")).click();

        try {
            Assert.assertEquals("Utilisateur", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        driver.get(urlRoot + "controllers/utilisateur.php");
        driver.findElement(By.id("nom")).sendKeys(initialNomPrenom);
        driver.findElement(By.id("prenom")).sendKeys(initialNomPrenom);
        driver.findElement(By.id("inputEmail")).sendKeys(initialEmail);
        driver.findElement(By.id("rad3")).click();
        driver.findElement(By.name("creer")).click();
        try {
            assertEquals("Utilisateur créée avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
            return;
        }

        driver.findElement(By.name("recherche")).sendKeys(initialNomPrenom);
        driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        driver.findElement(By.name("edit")).click();
        driver.findElement(By.id("nom")).clear();
        driver.findElement(By.id("nom")).sendKeys(modifiedNomPrenom);
        driver.findElement(By.id("prenom")).clear();
        driver.findElement(By.id("prenom")).sendKeys(modifiedNomPrenom);
        driver.findElement(By.id("inputEmail")).clear();
        driver.findElement(By.id("inputEmail")).sendKeys(modifiedEmail);
        driver.findElement(By.id("rad3")).click();
        driver.findElement(By.name("modifier")).click();
        try {
            assertEquals("Informations modifiées avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
            return;
        }

        driver.findElement(By.name("supprimer")).click();
        try {
            assertEquals("Utilisateur supprimé avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            logger.log(Level.SEVERE, verificationErrorString);
            fail(verificationErrorString);
        }
    }
}