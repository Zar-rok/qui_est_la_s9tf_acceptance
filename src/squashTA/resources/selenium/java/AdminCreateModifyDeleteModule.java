import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AdminCreateModifyDeleteModule {

    private final Logger logger = Logger.getLogger(AdminCreateModifyDeleteModule.class.getName());
    private WebDriver driver;
    private String urlRoot;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        urlRoot = "http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test4/preprod/Source/Vendor/";
    }

    @Test
    public void testAdminCreateModifyDeleteModule() {
        final String initialModule = "initialModule";
        final String initialEnseignant = "initialEnseignant";
        final String modifiedModule = "modifiedModule";
        final String modifiedEnseignant = "modifiedEnseignant";


        driver.get(urlRoot + "controllers/connexion.php");
        driver.findElement(By.id("idEmail")).sendKeys("testCO@co.ts");
        driver.findElement(By.id("idMotDePasse")).sendKeys("pouet");
        driver.findElement(By.name("submit")).click();

        driver.get(urlRoot + "controllers/module.php");
        driver.findElement(By.id("nom")).sendKeys(initialModule);
        driver.findElement(By.id("ensRef")).sendKeys(initialEnseignant);
        //new Select(driver.findElement(By.id("ensRef"))).selectByVisibleText(initialEnseignant);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Enseignant Référent'])[1]/following::option[2]")).click();
        driver.findElement(By.id("creer")).click();

        try {
            assertEquals("Module créée avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
            return;
        }

        driver.findElement(By.name("recherche")).sendKeys(initialModule);
        driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        driver.findElement(By.name("edit")).click();
        driver.findElement(By.id("nom")).clear();
        driver.findElement(By.id("nom")).sendKeys(modifiedModule);
        driver.findElement(By.name("modifier")).click();
        try {
            assertEquals("Module modifié avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
            return;
        }

        driver.findElement(By.name("supprimer")).click();
        try {
            assertEquals("Module supprimé avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            logger.log(Level.SEVERE, verificationErrorString);
            fail(verificationErrorString);
        }
    }
}