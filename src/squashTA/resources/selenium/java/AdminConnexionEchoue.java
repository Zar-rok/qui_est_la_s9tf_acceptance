import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.junit.Assert.fail;

public class AdminConnexionEchoue {


    protected HtmlUnitDriver driver;
    protected String urlRoot;
    protected StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        urlRoot = "http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test4/preprod/Source/Vendor/";
    }

    
    @Test
    public void testNotExistingUser() {
        driver.get(urlRoot+"controllers/connexion.php");
        driver.findElement(By.name("idEmail")).sendKeys("lol@gmail.com");
        driver.findElement(By.id("idMotDePasse")).sendKeys("test");
        driver.findElement(By.name("submit")).click();
        try {
            Assert.assertNotEquals("Utilisateur", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }


    @After
    public void tearDown() {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
    
    
}
